---
author: detrate
comments: false
date: 2010-12-23 20:18:51+00:00
slug: download
title: Download
wordpress_id: 839
type: page
---

### LATEST RELEASE: Xonotic 0.8.1 for Linux, Windows, and Mac

[Download via torrent](http://dl.xonotic.org/xonotic-0.8.1.zip.torrent)  
[Download via HTTP](http://dl.xonotic.org/xonotic-0.8.1.zip)  

No installation required, just unpack and run. Got questions? See the [FAQ](/faq) or [live chat](http://webchat.quakenet.org/?channels=xonotic) with us.

MD5: f7a9cd8ab68a00336acca164f983b569  
Package size: 940M  

### Upgrading from an older version

Since Xonotic version 0.5 we've offered an auto-update tool that will seamlessly upgrade your client to the latest release, provided you're using an official release or autobuild to begin with. You can find this tool in your Xonotic application directory. To use it, start by navigating to the misc->tools->rsync-updater subdirectory, then double-click (or otherwise execute in a terminal) the "update-to-release.sh" inside this directory. There is also a "update-to-release.bat" script for Windows users. This script will examine your installation and update only the files needed to bring your copy up to date using the "rsync" tool. In no time you'll have a shiny new client with which to play!

### Extra downloads

**[Download NetRadiant via HTTP](http://dl.xonotic.org/xonotic-0.8.1-mappingsupport.zip)**  
**Description:** Official Xonotic map editor with all the required textures to create maps. The download only contains binaries for Windows and OSX, Linux users will need to compile from source- which is also provided in the package.  

**[Lower quality download via HTTP](http://dl.xonotic.org/xonotic-0.8.1-low.zip)**  
**Description:** Release build with JPEG texture compression instead of using DDS textures compiled with S3TC. This build has smaller file size and has better support for opensource/legacy drivers, but the textures take slightly longer while loading the game.  

**[Source download via HTTP](http://dl.xonotic.org/xonotic-0.8.1-source.zip)**  
**Description:** Source of all code parts (also included with the other downloads).  

**[Our project page on GitLab](https://gitlab.com/groups/xonotic)**

### Older downloads  
[Download 0.8.0 via torrent](http://dl.xonotic.org/xonotic-0.8.0.zip.torrent) (953M, md5: bc368e116a2502362e1d4f07d8f8efab)  
[Download 0.8.0 via HTTP](http://dl.xonotic.org/xonotic-0.8.0.zip) (953M, md5: bc368e116a2502362e1d4f07d8f8efab)

[Download 0.7.0 via torrent](http://dl.xonotic.org/xonotic-0.7.0.zip.torrent) (993M, md5: eda7e8acadbefaf4b2efcfb70bbe98e2)  
[Download 0.7.0 via HTTP](http://dl.xonotic.org/xonotic-0.7.0.zip) (993M, md5: eda7e8acadbefaf4b2efcfb70bbe98e2)  

[Download 0.6.0 via torrent](http://dl.xonotic.org/xonotic-0.6.0.zip.torrent) (943M, md5: 2dac2c1ad4388255d3ad4d038dea3f77)  
[Download 0.6.0 via HTTP](http://dl.xonotic.org/xonotic-0.6.0.zip) (943M, md5: 2dac2c1ad4388255d3ad4d038dea3f77)  

[Download 0.5.0 via torrent](http://dl.xonotic.org/xonotic-0.5.0.zip.torrent) (943M, md5: cdadb384ccf9cad926bb377312832c2f)  
[Download 0.5.0 via HTTP](http://dl.xonotic.org/xonotic-0.5.0.zip) (943M, md5: cdadb384ccf9cad926bb377312832c2f)  

[Download 0.1.0 via torrent](http://dl.xonotic.org/xonotic-0.1.0preview.zip.torrent) (1.3G, md5: aafb43893aa66e01488c817e3a60d96d)  
[Download 0.1.0 via HTTP](http://dl.xonotic.org/xonotic-0.1.0preview.zip) (1.3G, md5: aafb43893aa66e01488c817e3a60d96d)  
